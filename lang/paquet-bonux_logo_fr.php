<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bonux_logo_description' => 'Un spip bonux, juste pour les logos <br/> Auteur du logo <a href="https://thenounproject.com/search/?q=image&i=625935">Robert Kyriakis</a>',
	'bonux_logo_nom' => 'Bonux des logo',
	'bonux_logo_slogan' => 'Des fonctions additionnelles pour gérer les logos de SPIP',
);
